# run this to make it work:
# PYTHONPATH=/home/training/crockode/src/crockode pytest

from crockode import encode_string, decode_dna
import pytest

def test_decode_dna_ASCII():
    all_nt = 'ATGC'
    decoded_message = decode_dna(all_nt)
    encoded_message = encode_string(decoded_message)
    assert all_nt == encoded_message

def test_decode_dna_hello():
    assert decode_dna("GCCAGCGGGCTAGCTAGCTT") == "hello"

def test_decode_empty_string():
    assert decode_dna("") == ""

def test_decode_numerical_string():
    assert decode_dna("526") == ""

def test_decode_binary_string():
    assert decode_dna("0101111011") == ""

def test_exception_numerical_input():
    with pytest.raises(ExceptionGroup) as excinfo:
        decode_dna(526)
    assert excinfo.group_contains(TypeError)

def test_exception_none_input():
    with pytest.raises(ExceptionGroup) as excinfo:
        decode_dna(None)
    assert excinfo.group_contains(TypeError)
# run this to make it work:
# PYTHONPATH=/home/training/crockode/src/crockode pytest

from crockode import encode_string, decode_dna

def test_encode_string_all_ascii():
    all_ascii = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
    encoded_message = encode_string(all_ascii)
    decoded_message = decode_dna(encoded_message)
    assert all_ascii == decoded_message

def test_encode_string_hello():
    assert encode_string("hello") == "GCCAGCGGGCTAGCTAGCTT"
